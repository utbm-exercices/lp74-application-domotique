# LP74 Projet IHM - Liens utilisés pour les recherches

## Sociologie

### Utilisation des technologies en France

- [Etude sur l'utilisation des smartphones en France](https://fr.statista.com/themes/2758/l-utilisation-des-smartphones-en-france/#dossierSummary__chapter1)
- [Utilisation des smartphones en France selon les tranches d'âges](https://fr.statista.com/statistiques/505110/taux-de-penetration-du-smartphone-par-age-france/)
- [Evolution de la possession des téléphones en France entre 2011 et 2019](https://fr.statista.com/statistiques/505125/types-de-telephones-mobiles-utilises-france/)
- [Evolution des achats sur internet entre 2009 et 2019 selon des tranches d'âges](https://www.insee.fr/fr/statistiques/2498792)
- [Rassemblement de statistiques sur l'utilisation de l'informatique](https://insee.fr/fr/statistiques/3324829?sommaire=3324839)
- [Possession de résidence secondaire](https://immobilier.lefigaro.fr/article/residences-secondaires-les-francais-mettent-le-cap-a-l-ouest_e8f02124-6155-11e7-8eb8-e1cec1c7e2c5/)

## Technique

- [Application Imperihome pour centraliser la domotique](https://www.maison-et-domotique.com/35382-imperihome-lapplication-domotique-pour-centraliser/)
- [Logiciels domotique OpenSource](https://projetsdiy.fr/logiciel-domotique-open-source-projets-diy/)
