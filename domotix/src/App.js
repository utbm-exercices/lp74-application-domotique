import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router} from "react-router-dom";
import Routes from './components/routing/Routes';

import Header from "./components/layout/Header";

const rooms = [
    {
        index: 0,
        name: 'Chambre de Adrien',
        type: 'sleepingRoom',
        temperature: 21,
        light: true,
    },
    {
        index: 1,
        name: 'Chambre de Léo',
        type: 'sleepingRoom',
        temperature: 22,
        light: false,
    },
    {
        index: 2,
        name: 'Chambre des parents',
        type: 'sleepingRoom',
        temperature: 21,
        light: false,
    },
    {
        index: 3,
        name: 'Chambre d\'amis',
        type: 'sleepingRoom',
        temperature: 21,
        light: false,
    },
    {
        index: 4,
        name: 'Salle de bain',
        type: 'waterRoom',
        temperature: 21,
        light: false,
    },
    {
        index: 5,
        name: 'Toilettes',
        type: 'waterRoom',
        temperature: 21,
        light: false,
    },
    {
        index: 6,
        name: 'Cuisine',
        type: 'eatingRoom',
        temperature: 21,
        light: false,
    },
    {
        index: 7,
        name: 'Salle à manger',
        type: 'eatingRoom',
        temperature: 21,
        light: false,
    },
    {
        index: 8,
        name: 'Bureau',
        type: 'other',
        temperature: 21,
        light: false,
    },
    {
        index: 9,
        name: 'Salle de jeux',
        type: 'other',
        temperature: 21,
        light: false,
    },
    {
        index: 10,
        name: 'Salon',
        type: 'other',
        temperature: 21,
        light: false,
    },
    {
        index: 11,
        name: 'Garage',
        type: 'other',
        temperature: 21,
        light: false,
    },
    {
        index: 12,
        name: 'Extérieur',
        type: 'other',
        temperature: 21,
        light: false,
    },
]

class App extends Component{
  render() {
    return (
        <div className="App">
            <Router>
                <Header/>
                <Routes rooms={rooms}/>
            </Router>
        </div>
    );
  }
}

export default App;
