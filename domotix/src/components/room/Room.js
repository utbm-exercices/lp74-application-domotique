import React from "react";
import {faBed, faTint, faSun, faUtensils, faPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

class Room extends React.Component {
    state = {
        room: null,
    }

    constructor(props) {
        super(props);

        this.state.room = props.room;
    }

    render() {
        return (
            <div className="room"
                 style={
                    this.state.room.type === 'sleepingRoom' ?
                        {color: 'purple'} :
                    this.state.room.type === 'waterRoom' ?
                        {color: 'blue'} :
                    this.state.room.type === 'eatingRoom' ?
                        {color: 'green'}:
                    // Default
                        {color: 'black'}}
                 onClick={(e) => this.props.handleClick(e, this.state.room)}
            >
                <div>
                    <p>{this.state.room.name}</p>
                </div>
                <div>
                    {this.state.room.type === "sleepingRoom" ?
                        (<FontAwesomeIcon icon={faBed}/>) :
                    this.state.room.type === "waterRoom" ?
                        (<FontAwesomeIcon icon={faTint}/>) :
                    this.state.room.type === "eatingRoom" ?
                        (<FontAwesomeIcon icon={faUtensils}/>) :
                    this.state.room.type === "addCase" ?
                        (<FontAwesomeIcon icon={faPlus}/>) :
                    // Default
                        (<FontAwesomeIcon icon={faSun}/>)
                    }
                </div>
            </div>
        );
    }
}

export default Room;