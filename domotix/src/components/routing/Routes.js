import React, {Component} from "react";
import {Route, Switch} from 'react-router-dom';

import RoomGrid from "../roomGrid/RoomGrid";

class Routes extends Component {
    state = {
        rooms: null,
    }

    constructor(props) {
        super (props);
        this.state.rooms = props.rooms;
    }

    render() {
        return (
            <Switch>
                <Route path="/" exact>
                    <RoomGrid rooms={this.state.rooms}/>
                </Route>
            </Switch>
        );
    }
}

export default Routes;
