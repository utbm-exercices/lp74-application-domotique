import React from 'react';
import './RoomGrid.css';
import Grid from "@material-ui/core/Grid";
import Room from "../room/Room";
import RoomView from "../roomView/RoomView";


class RoomGrid extends React.Component {
    state = {
        selectedRoom: null,
        rooms: null,
    };

    constructor(props) {
        super (props);
        this.state.rooms = props.rooms;
    }
    selectRoom = (event, room) => {
        this.setState((previousState) => { return {...previousState, selectedRoom: room} });
    }

    unselectRoom = () => {
        this.setState((previousState) => { return {...previousState, selectedRoom: null} });
    }

    render() {
        let caseNumber = 0;
        let gridList = this.state.rooms.map(room => {
                return  <Grid item xs={4} className="grid-element" key={caseNumber++}>
                            <Room room={room} handleClick={this.selectRoom}/>
                        </Grid>;
            }
        );

        gridList.push(
            <Grid item xs={4} className="grid-element" key={caseNumber}>
                <Room room={{name: 'Ajouter une pièce', type: 'addCase'}}/>
            </Grid>);
        caseNumber++;

        for (caseNumber; caseNumber % 3 !== 0; caseNumber++)
            gridList.push(<Grid item xs={4} className="grid-element" key={caseNumber}> </Grid>);

        return <>
            <div className='scroll'>
                <Grid container className="room-grid">
                    {gridList}
                </Grid>
            </div>
            {
                this.state.selectedRoom !== null ?
                    <RoomView room={this.state.selectedRoom} onClose={this.unselectRoom}/> :
                    null
            }
        </>;
    }
}

export default RoomGrid;