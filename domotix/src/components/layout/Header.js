import React, { Component } from 'react';
import './Header.css';
import Grid from "@material-ui/core/Grid";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons";

class Header extends Component {

    state = {collapse: null};

    constructor(props) {
        super(props);

        this.state.collapse = true;
    }

    render() {
        return (
            <Grid container className='header'>
                <Grid container item>
                    <Grid item xs={1}>
                        <FontAwesomeIcon icon={faBars} size="2x"/>
                    </Grid>
                    <Grid item xs={11} className="title">
                        <p><span>&lt;Nom de résidence&gt;</span></p>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

export default Header;
