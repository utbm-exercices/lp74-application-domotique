import React from 'react';
import {Grid, Button} from '@material-ui/core';
import {faTimesCircle, faThermometerThreeQuarters, faLightbulb as faLightbulbOn} from "@fortawesome/free-solid-svg-icons";
import {faLightbulb as faLightbulbOff} from "@fortawesome/free-regular-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import './RoomView.css';

class RoomView extends React.Component {

    state = {
        room: null,
    };

    constructor(props) {
        super (props);
        this.state.room = props.room;
    }

    render() {
        return (
        <div className='room-view'>
            <Grid container>
                <Grid item xs={11}>
                    <p>{this.state.room.name}</p>
                </Grid>
                <Grid item xs={1}>
                    <FontAwesomeIcon icon={faTimesCircle} onClick={() => this.props.onClose()}/>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={4}>
                    <FontAwesomeIcon icon={faThermometerThreeQuarters}/>
                    <p>{this.state.room.temperature}°C</p>
                </Grid>
                <Grid item xs={4}>
                    {
                        this.state.room.light ?
                            (<FontAwesomeIcon icon={faLightbulbOn}/>) :
                            (<FontAwesomeIcon icon={faLightbulbOff}/>)
                    }

                </Grid>
                <Grid item xs={4}>
                    <Button variant='contained' color='default'>Modifier</Button>
                </Grid>
            </Grid>
        </div>
        );
    }
}

export default RoomView;